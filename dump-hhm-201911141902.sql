-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: hhm
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `table_messages`
--

DROP TABLE IF EXISTS `table_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table_messages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` text COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` text COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `table_messages`
--

LOCK TABLES `table_messages` WRITE;
/*!40000 ALTER TABLE `table_messages` DISABLE KEYS */;
INSERT INTO `table_messages` VALUES (1,'Thom','Vial','thom.v@some.com'),(2,'Thom','Vial','thom.v@some.com'),(3,'Thom','Vial','thom.v@some.com'),(4,'Thom','Vial','thom.v@some.com'),(5,'Thom','Vial','thom.v@some.com'),(6,'Thom','Vial','thom.v@some.com'),(7,'aa','sed@aerdh.ru',''),(8,'awsdb','stretduuiytruye@setj.ew','serstryjm\r\ntest'),(9,'WEG','RAETDFGTS@sfrgtngt.ru','waerbgwerhg'),(10,'aewsrh','aewaew@saedrt.ru','aqewrhqa'),(11,'aawsha','aehsaetrh@aedrtr.ru','awRHG'),(12,'aawsha','aehsaetrh@aedrtr.ru','awRHG'),(13,'aawsha','aehsaetrh@aedrtr.ru','awRHG'),(14,'aawsha','aehsaetrh@aedrtr.ru','awRHG'),(15,'aawsha','aehsaetrh@aedrtr.ru','awRHG'),(16,'awerhb','aeaerb@aettr.ru','aeraqetr aebgtr'),(17,'wAEAWRF','aeaestn@azsr.ru','adsads er\r\naewrg \r\nerg'),(18,'zsbzs','ddtn@asedrh.ru','srtsr\r\netrdserdn\r\nadn');
/*!40000 ALTER TABLE `table_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'hhm'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-14 19:02:42
