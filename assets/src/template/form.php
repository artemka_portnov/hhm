<?php ?>
<div class="c-form">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-12">
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-sm-8 col-12">
                        <a href="https://honey-hunters.ru/" target="_blank">
                            <img src="/img/logo.png" alt="" class="mw-100">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <form method="POST" action="" name="form" class="w-100 needs-validation" novalidate>
                <div class="col-xl-11 col-12">
                    <div class="c-form__ico">
                        <img src="/img/icon.png" alt="">
                    </div>
                </div>
                <div class="main-row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="form-group required">
                            <label for="inputName">Имя</label>
                            <input type="text" class="form-control" id="inputName" name="inputName" required>
                            <div class="invalid-feedback">
                                Заполните поле
                            </div>
                        </div>
                        <div class="form-group required">
                            <label for="inputEmail">E-Mail</label>
                            <input type="email" class="form-control" id="inputEmail" name="inputEmail" required>
                            <div class="invalid-feedback">
                                E-mail введен неверно
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 offset-xl-1 col-md-12">
                        <div class="form-group required form-group_textarea">
                            <label for="inputMessage">Комментарий</label>
                            <textarea class="form-control" id="inputMessage" name="inputMessage" required></textarea>
                            <div class="invalid-feedback">
                                Оставьте комментарий
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-right">
                        <button type="submit" class="btn" id="sub">Записать</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>