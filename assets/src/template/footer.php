<?php ?>
<footer class="c-footer">
    <div class="container">
        <div class="row">
            <!--<div class="col-11">
                <div class="row">-->
                    <div class="col-xl-3 col-lg-4 col-md-5 col-sm-6 c-footer__logo">
                        <a href="https://honey-hunters.ru/" target="_blank">
                            <img src="/img/logo.png" alt="" class="w-100">
                        </a>
                    </div>
                <!--</div>
            </div>-->
            <div class="offset-xl-7 offset-lg-6 offset-md-3 offset-sm-2 col-lg-1 col-sm-2 col-3 c-social">
                <a href="javascript:void(0);">&#xf189;</a>
            </div>
            <div class="col-lg-1 col-sm-2 col-3 c-social">
                <a href="javascript:void(0);">&#xf09a;</a>
            </div>
        </div>
    </div>
</footer>
