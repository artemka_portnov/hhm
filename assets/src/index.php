<?php ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/main.min.css">
    <title>HoneyHunters</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet">
    <script src="js/main.min.js"></script>
</head>
<body>

<?include "template/form.php"?>

<?include "template/items.php"?>

<?include "template/footer.php"?>

</body>
</html>