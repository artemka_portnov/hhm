'use strict';
$(document).ready( function() {
    var forms = $('.needs-validation');
    var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
            console.log(form.checkValidity())
            event.preventDefault();
            form.classList.add('was-validated');
            if (form.checkValidity() === false) {
                event.stopPropagation();
            }
            else {
                var name = $('#inputName'),
                    email = $('#inputEmail'),
                    message = $('#inputMessage');

                $.ajax({
                        type: "POST",
                        url: "/template/ajax.php",
                        data: {
                            inputName: name.val(),
                            inputEmail: email.val(),
                            inputMessage: message.val(),
                            ajax: true
                        },
                        success: function (response) {
                            $('#ajax').html(response);
                            name.val("");
                            email.val("");
                            message.val("");
                            form.classList.remove('was-validated');
                        }
                    }
                );
            }
        }, false);
    });
});